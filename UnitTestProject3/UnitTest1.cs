using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using WebService;
using WebService.Controllers;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod__For_V1()
        {
            BalanceController obj = new BalanceController();
            var jsonString = File.ReadAllText("balanceV1.json");
            var res = obj.GetBalance(JsonConvert.DeserializeObject<InputData>(jsonString));
            var succes = res.succes;
            var result = res.results;

            var etalone = new double[]
            {
                10.5402456913603,
                2.83614833622227,
                6.97261297632469,
                1.9632643552402,
                5.0093486210845,
                4.02033457294678,
                0.989014048137712,
                0.731484378813389
            };
            for (int i = 0; i < result.Length; i++)
            {
                Assert.AreEqual(Math.Round(result[i], 2), Math.Round(etalone[i], 2));
            }
        }

        [TestMethod]
        public void TestMethod_For_Orign()
        {
            BalanceController obj = new BalanceController();
            var jsonString = File.ReadAllText("balanceOrign.json");
            var res = obj.GetBalance(JsonConvert.DeserializeObject<InputData>(jsonString));
            var succes = res.succes;
            var result = res.results;

            var etalone = new double[]
            {
                10.0555820555568,
                3.0142509913592,
                7.04133106419761,
                1.98210405567851,
                5.0592270085191,
                4.06740355102557,
                0.991823457493527
            };
            for (int i = 0; i < result.Length; i++)
            {
                Assert.AreEqual(Math.Round(result[i], 2), Math.Round(etalone[i], 2));
            }
        }

        [TestMethod]
        public void TestMethod_For_NonSolve()
        {
            BalanceController obj = new BalanceController();
            var jsonString = File.ReadAllText("balanceNonSolve.json");
            var res = obj.GetBalance(JsonConvert.DeserializeObject<InputData>(jsonString));
            var succes = res.succes;
            var result = res.results;

            var etalone = false;
            
            Assert.AreEqual(succes, etalone);
            
        }
    }
}
