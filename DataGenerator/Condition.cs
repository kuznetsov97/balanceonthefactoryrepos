﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataGenerator
{
    public struct Condition
    {
        public int count;
        public int[] variablesAtIndices;
        public double[] combinedAs;
        public int shouldBe;
        public double value;
    }
}
