﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGenerator
{
    public class Node
    {
        public Node(string _id, double _y)
        {
            id = _id;
            y = _y;
            sourceFlowsIds = new List<string>();
            destinFlowsIds = new List<string>();
        }

        public string id;
        public double y;
        public List<string> sourceFlowsIds;
        public List<string> destinFlowsIds;
    }
}
