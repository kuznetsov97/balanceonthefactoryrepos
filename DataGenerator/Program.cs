﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGenerator
{
    class Program
    {
        struct forJson
        {
            public Condition[] conditions;
            public Node[] nodes;
            public Flow[] flows;
        };

        static void Main(string[] args)
        {
            Console.WriteLine("Генератор модели для задачи о балансе на предприятии");
            Console.WriteLine("Введите кол-во узлов:");
            var countNodes = Convert.ToInt32(Console.ReadLine());
            var countFlows = 0;
            while (countFlows < countNodes)
            {
                Console.WriteLine("Введите кол-во потоков > узлов:");
                countFlows = Convert.ToInt32(Console.ReadLine());
                if (countFlows < countNodes) Console.WriteLine("Кол-во потоков не должно быть меньше, чем узлов");
            }
            var _flows = new Flow[countFlows];
            var _nodes = new Node[countNodes];                       

            RandomGeneration(countNodes, countFlows,  ref _nodes, ref _flows);

            var json = JsonConvert.SerializeObject(new forJson { nodes = _nodes, flows = _flows, conditions = new Condition[0] });

            Console.WriteLine("Данные сгенерированы\nВведите название файла для сохранения:");
            var fileName = Console.ReadLine();
            using (var f = new StreamWriter(fileName + ".json", true))
            {
                f.Write(json);
            }

        }

        //Генерация связей и величин потоков, погрешностей и прочего
        public static void RandomGeneration(int countNodes, int countFlows, ref Node[] nodes, ref Flow[] flows)
        {
            var rand = new Random();

            for(int i = 0; i < countFlows; i++)
            {
                flows[i] = new Flow(Guid.NewGuid().ToString(), (double)rand.Next(0, 100)/100, rand.Next(0, 1000) + (double)rand.Next(100, 900)/100, "true", 2000, -1001);
            }

            for (int i = 0; i < countNodes; i++)
            {
                
                nodes[i] = new Node(Guid.NewGuid().ToString(), 0);

                //Последовательная связь всех узлов в цель
                if (i == 0)
                    nodes[i].destinFlowsIds.Add(flows[i].id);
                else
                {
                    nodes[i - 1].sourceFlowsIds.Add(flows[i].id);
                    nodes[i].destinFlowsIds.Add(flows[i].id);
                }
            }
            //Абсолютно случайные направления потоков из узлов
            for(int i = countNodes; i < countFlows; i++)
            {
                if(rand.Next(0,2) == 1)
                    nodes[rand.Next(0, countNodes - 1)].destinFlowsIds.Add(flows[i].id);
                if(rand.Next(0,2) == 1)
                    nodes[rand.Next(0, countNodes - 1)].sourceFlowsIds.Add(flows[i].id);
            }
        }
    }
}
