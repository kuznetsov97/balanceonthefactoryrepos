﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGenerator
{
    public class Flow
    {
        public Flow(string _id, double _tolerance, double _measured, string _isMeasured, double up, double low)
        {
            id = _id;
            name = "flow_" + id;
            metrologicRange = new range { min = low, max = up};
            tolerance = _tolerance;
            measured = _measured;
            isMeasured = _isMeasured;
        }

        public string id;
        public string name;
        public range metrologicRange;
        public double tolerance;
        public double measured;
        public string isMeasured;
    }

    public struct range
    {
        public double min;
        public double max;
    }
}
