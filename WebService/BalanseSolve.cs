﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accord.Math.Optimization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace WebService
{
    public class BalanseSolve
    {    
        public OutputData ReturnResult(InputData input)
        {
            var param = Parser(input);
            if(param.conditions.Count != 0)
            {
                foreach(var item in param.conditions)
                {
                    AddConstrain(item.count, item.variablesAtIndices, item.combinedAs, item.shouldBe, item.value);
                }
            }
            double[] _result;
            var _succes = Solving(param.Means, param.t, param.x0, param.b, param.A, param.up, param.low, out _result);
            
            return new OutputData { succes = _succes, results = _result };          
        }

        /// <summary>
        /// Парсер входящих данных формата json
        /// </summary>
        /// <param name="args">входные данные в виде InputData</param>
        /// <returns>структура из необходимых для вычилсения данных</returns>
        private parametrs Parser(InputData args)
        {
            List<Flow> flows = args.flows.ToList();
            List<Node> nodes = args.nodes.ToList();
            List<Conditioon> conditions = args.conditions.ToList();

            var y0 = new double[nodes.Count];                      
            var isMeans = new int[flows.Count];
            var means = new double[flows.Count];
            var tol = new double[flows.Count];
            var upperLimit = new double[flows.Count];
            var lowerLimit = new double[flows.Count];

            for (int i = 0; i < nodes.Count; i++)
                y0[i] = nodes[i].y;
            for (int i = 0; i < flows.Count; i++)
            {
                lowerLimit[i] = flows[i].metrologicRange.min;
                upperLimit[i] = flows[i].metrologicRange.max;
                means[i] = flows[i].measured;
                isMeans[i] = (flows[i].isMeasured == "true")? 1 : 0;      
                tol[i] = flows[i].tolerance;               
            }

            var Matrix = new double[nodes.Count][];
            for(int i = 0; i < nodes.Count; i++)
            {
                Matrix[i] = new double[flows.Count];
                for(int j = 0; j < flows.Count; j++)
                {
                    var idCurrentFlow = flows[j].id;
                    var source = nodes[i].sourceFlowsIds;
                    var destination = nodes[i].destinFlowsIds;
                    foreach (var s in source)
                    {
                        if(s == idCurrentFlow)
                            Matrix[i][j] = -1;
                    }
                    foreach (var d in destination)
                    {
                        if (d == idCurrentFlow)
                            Matrix[i][j] = 1;
                    }

                }
            }

            return new parametrs
            {
                Means = isMeans,
                t = tol,
                x0 = means,
                b = y0,
                A = Matrix,
                up = upperLimit,
                low = lowerLimit,
                conditions = conditions
            };
        }

        /// <summary>
        /// Нахождение баланса
        /// </summary>
        /// <param name="measurability">индикаторы изменяемости</param>
        /// <param name="t">метрологические погрешности</param>
        /// <param name="x0">измереные значения по каждому потоку</param>
        /// <param name="b">правая часть балансовой матрицы</param>
        /// <param name="A">матрица потоков</param>
        /// <param name="lower">нижняя граница измерений</param>
        /// <param name="upper">верхняя граница измерений</param>
        /// <param name="solution">сбалансированные потоки</param>
        /// <returns>успех операции</returns>
        public bool Solving(int[] measurability, double[] t, double[] x0, double[] b, double[][] A, double[] upper, double[] lower, out double[] solution)
        {
            List<LinearConstraint> constraints;
            if (Constraints.Count != 0)
                constraints = Constraints;
            else
                constraints = new List<LinearConstraint>();

            var count = measurability.Length;

            var I = new double[count, count];//диагональная матрица размера count*count, где на диагонали стоят индикаторы измеряемости
            var W = new double[count, count];//диагональная матрица размера  count*count, где на диагонали стоят веса.
            for (int i = 0; i < t.Length; i++)
            {
                W[i, i] = t[i] != 0 ? (1 / (t[i] * t[i])) : 0;
                I[i, i] = measurability[i];
            }

            var H = Accord.Math.Matrix.Dot(W, I);

            var d = new double[count];

            for (int i = 0; i < count; i++)
            {
                d[i] = -1 * H[i, i] * x0[i];
            }

            var VariablesAtIndices = new int[count];
            for (int i = 0; i < VariablesAtIndices.Count(); i++)
                VariablesAtIndices[i] = i;

            for(int i = 0; i < count; i++)
            { 
                //Условие нижней границы
                constraints.Add(new LinearConstraint(numberOfVariables: 1)
                {
                    VariablesAtIndices = new int[] { i },
                    ShouldBe = ConstraintType.GreaterThanOrEqualTo,
                    Value = lower[i]
                });
                //Условие верхней границы
                constraints.Add(new LinearConstraint(numberOfVariables: 1)
                {
                    VariablesAtIndices = new int[] { i },
                    ShouldBe = ConstraintType.LesserThanOrEqualTo,
                    Value = upper[i]
                });
            }

            //вектора Ах = b, где b = {0,..,0}
            for (int i = 0; i < A.Count(); i++)
            {
                constraints.Add(new LinearConstraint(numberOfVariables: count)
                {
                    VariablesAtIndices = VariablesAtIndices,
                    CombinedAs = A[i],
                    ShouldBe = ConstraintType.EqualTo,
                    Value = b[i]
                });
            }

            var solver = new GoldfarbIdnani(
                function: new QuadraticObjectiveFunction(H, d),
                constraints: constraints
                );

            bool success = solver.Minimize();
            solution = solver.Solution;
            return success;
        }

        List<LinearConstraint> Constraints = new List<LinearConstraint>();
        public void AddConstrain(int count, int[] VariablesAtIndices, double[] combinedAs, int shouldBe, double value)
        {
            Constraints.Add(new LinearConstraint(numberOfVariables: count)
            {
                VariablesAtIndices = VariablesAtIndices,
                CombinedAs = combinedAs,
                ShouldBe = (ConstraintType)shouldBe,
                Value = value
            });
        }
    }

    /// <summary>
    /// Структура для входных параметров
    /// </summary>
    public struct parametrs
    {
        public int[] Means;
        public double[] t;
        public double[] x0;
        public double[] b;
        public double[][] A;
        public double[] up;
        public double[] low;
        public List<Conditioon> conditions;
    }
}

