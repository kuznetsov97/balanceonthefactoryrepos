﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace WebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BalanceController : ControllerBase
    {
        // POST api/balance
        [HttpPost]
        public OutputData GetBalance([FromBody]InputData value)
        {   
            return new BalanseSolve().ReturnResult(value);
        }     
    }

}
