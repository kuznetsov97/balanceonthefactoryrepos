﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService
{
    public struct InputData
    {
        public Conditioon[] conditions { get; set; }
        public Node[] nodes { get; set; }
        public Flow[] flows { get; set; }
    };
}
