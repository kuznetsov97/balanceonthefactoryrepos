﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService
{
    public struct Conditioon
    {
        public int count { get; set; }
        public int[] variablesAtIndices { get; set; }
        public double[] combinedAs { get; set; }
        public int shouldBe { get; set; }
        public double value { get; set; }
    }
}
