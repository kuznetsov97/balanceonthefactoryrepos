﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebService
{
    public class Node
    {
        public Node(string _id, double _y)
        {
            id = _id;
            y = _y;
            sourceFlowsIds = new List<string>();
            destinFlowsIds = new List<string>();
        }

        public string id { get; set; }
        public double y { get; set; }
        public List<string> sourceFlowsIds { get; set; }
        public List<string> destinFlowsIds { get; set; }
    }
}
