﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebService
{
    public class Flow
    {
        public Flow(string _id, double _tolerance, double _measured, string _isMeasured, double up, double low)
        {
            id = _id;
            name = "flow_" + id;
            metrologicRange = new range { min = low, max = up};
            tolerance = _tolerance;
            measured = _measured;
            isMeasured = _isMeasured;
        }

        public string id { get; set; }
        public string name { get; set; }
        public range metrologicRange { get; set; }
        public double tolerance { get; set; }
        public double measured { get; set; }
        public string isMeasured { get; set; }
    }

    public struct range
    {
        public double min { get; set; }
        public double max { get; set; }
    }
}
