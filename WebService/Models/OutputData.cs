﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService
{
    public struct OutputData
    {
        public bool succes { get; set; }
        public double[] results { get; set; }
    };
}
